/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package locationvoiture;

/**
 *
 * @author Bobo
 */
    public class Voiture {
        
    final private int NB_PLACE;
    private String modele;
    
    public Voiture(String modele, int nbPlace){
        this.modele = modele;
        this.NB_PLACE = nbPlace;
    }

    public String getModele() {
        return modele;
    }

    public int getNB_PLACE() {
        return NB_PLACE;
    }
    
    
}
