/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package locationvoiture;

/**
 *
 * @author Bobo
 */
public class MembreGold extends MembreNormal {
    
    private final int prixGold = 5;
    

    public MembreGold(String nom) {
        super(nom);
    }
   
    @Override
    public int calculerLocationPour(Voiture voiture) {
        
        return prixGold*voiture.getNB_PLACE();
    }
    
    
    
    
}
