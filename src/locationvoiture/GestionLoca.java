/*
 */
package locationvoiture;
import java.util.ArrayList;
import java.util.Arrays;
/**
 *
 * @author Bobo
 */
public class GestionLoca {
    
    private Voiture tabVoiture []={
        new Voiture("Smart 4 Two", 2), 
        new Voiture("Smart 4 Four", 4), 
        new Voiture("Renault Megane",5),
        new Voiture("Renault Espace",6)};
    
    public ArrayList<Voiture> VOITURES=new ArrayList(Arrays.asList(tabVoiture));
    public ArrayList<MembreNormal> CLIENTS = new ArrayList(Arrays.asList());
    public ArrayList<Reservation> RESERVATIONS = new ArrayList();

    
    public ArrayList<MembreNormal> getCLIENTS() {
        return CLIENTS;
    }

    public ArrayList<Reservation> getRESERVATIONS() {
        return RESERVATIONS;
    }

    public ArrayList<Voiture> getVOITURES() {
        return VOITURES;
    }
    
    public void ajouterClient(MembreNormal inscriptionClient){
        CLIENTS.add(inscriptionClient);
    }
    public void ajouterReservation(Reservation reservation){
        VOITURES.remove(reservation.getVoiture());
        RESERVATIONS.add(reservation);
    }
}
    

