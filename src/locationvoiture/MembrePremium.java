/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package locationvoiture;

/**
 *
 * @author Bobo
 */   
    public class MembrePremium extends MembreNormal {
    
    private final int prixPremium = 5;

    public MembrePremium(String nom) {
        
        super(nom);
    }
    @Override
    public int calculerLocationPour(Voiture voiture) {
        
        return 50+(prixPremium*voiture.getNB_PLACE());
    }
    
}
